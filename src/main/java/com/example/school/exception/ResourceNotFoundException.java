package com.example.school.exception;

public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException(String resource) {
        super(resource + " not found!");
    }
}