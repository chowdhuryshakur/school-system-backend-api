package com.example.school.service;

import com.example.school.exception.ResourceAlreadyExistsException;
import com.example.school.exception.ResourceNotFoundException;
import com.example.school.model.Class;
import com.example.school.repository.ClassRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public  class ClassService {


    private ClassRepository classRepository;

    public ClassService(ClassRepository classRepository) {
        this.classRepository = classRepository;
    }

    public Class insertClass(Class classs) throws ResourceAlreadyExistsException {
        Optional<Class> optionalClass = classRepository.findById(classs.getClassId());
        if(optionalClass.isPresent()){
            throw new ResourceAlreadyExistsException(classs.getClassId() + "");
        }
        else{
            return classRepository.save(classs);
        }
    }

    public Class updateClass (String classId, Class classs) throws ResourceNotFoundException {
        Optional<Class> optionalClass = classRepository.findById(classId);
        if(optionalClass.isPresent()){
            classs.setClassId(classId);
            return classRepository.save(classs);
        }
        else {
            throw new ResourceNotFoundException(classId + "");
        }

    }

    public Class findById(String classId) throws ResourceNotFoundException {
        Optional<Class> optionalClass = classRepository.findById(classId);
        if(optionalClass.isPresent()){
            return optionalClass.get();
        }else {
            throw new ResourceNotFoundException(classId + "");
        }
    }

    public List<Class> findAll(){
        List<Class> classList = new ArrayList<>();
        classRepository.findAll().forEach(classList::add);
        return classList;
    }

    public  boolean deleteById(String classId) throws ResourceNotFoundException{
        Optional<Class> optionalClass = classRepository.findById(classId);
        optionalClass.ifPresent(classs -> classRepository.deleteById(classId));
        optionalClass.orElseThrow(() -> new ResourceNotFoundException(classId + ""));
        return true;
    }



}
