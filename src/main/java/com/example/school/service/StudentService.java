package com.example.school.service;


import com.example.school.exception.ResourceAlreadyExistsException;
import com.example.school.exception.ResourceNotFoundException;
import com.example.school.model.Student;
import com.example.school.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public  class StudentService {

    private StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Student insertStudent(Student student) throws ResourceAlreadyExistsException {
        Optional<Student> optionalStudent = studentRepository.findById(student.getStudentId());
        if(optionalStudent.isPresent()) {
            throw new ResourceAlreadyExistsException(student.getStudentId() + "");
        }
        else{
            return studentRepository.save(student);
        }
    }

    public Student updateStudent (String studentId, Student student) throws ResourceNotFoundException {
        Optional<Student> optionalStudent = studentRepository.findById(studentId);
        if(optionalStudent.isPresent()){
            student.setStudentId(studentId);
            return studentRepository.save(student);
        }
        else {
            throw new ResourceNotFoundException(studentId + "");}
    }

    public Student findById(String studentId) throws ResourceNotFoundException {
        Optional<Student> optionalStudent = studentRepository.findById(studentId);
        if(optionalStudent.isPresent()){
            return optionalStudent.get();
        }else {
            throw new ResourceNotFoundException(studentId + "");
        }
    }

    public List<Student> findAll(){
        List<Student> studentList = new ArrayList<>();
        studentRepository.findAll().forEach(studentList::add);
        return studentList;
    }

    public  boolean deleteById(String studentId) throws ResourceNotFoundException{
        Optional<Student> optionalStudent = studentRepository.findById(studentId);
        optionalStudent.ifPresent(student -> studentRepository.deleteById(studentId));
        optionalStudent.orElseThrow(() -> new ResourceNotFoundException(studentId + ""));
        return true;
    }



}
