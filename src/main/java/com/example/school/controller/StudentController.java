package com.example.school.controller;

import com.example.school.exception.ResourceAlreadyExistsException;
import com.example.school.exception.ResourceNotFoundException;
import com.example.school.model.Student;
import com.example.school.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("")
    public ResponseEntity<List<Student>> getStudent(){
        List<Student> studentList = studentService.findAll();
        return ResponseEntity.ok(studentList);
    }

    @GetMapping("/{studentId}")
    public ResponseEntity<Student> getStudent(@PathVariable String studentId){
        try{
            Student student = studentService.findById(studentId);
            return ResponseEntity.ok(student);}
        catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception f){
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("")
    public  ResponseEntity<Student> insertStudent(@RequestBody Student student){
        try{
            Student insertStudent = studentService.insertStudent(student);
            return ResponseEntity.status(HttpStatus.CREATED).body(insertStudent);}
        catch (ResourceAlreadyExistsException e) {return ResponseEntity.badRequest().body(null);}

    }
    @PutMapping("/{studentId}")
    public  ResponseEntity<Student> updateStudent(@PathVariable String studentId, @RequestBody Student student){
        try{
            Student updateStudent = studentService.updateStudent(studentId, student);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(updateStudent);}
        catch (ResourceNotFoundException e) {return ResponseEntity.badRequest().body(null);}

    }

    @DeleteMapping("/{studentId}")
    public ResponseEntity<String> deleteStudent(@PathVariable String studentId){
        try{
            boolean deletedStudent = studentService.deleteById(studentId);
            return ResponseEntity.ok(studentId);}
        catch (ResourceNotFoundException e){return ResponseEntity.notFound().build();}
    }



}