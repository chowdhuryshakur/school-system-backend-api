package com.example.school.controller;


import com.example.school.exception.ResourceAlreadyExistsException;
import com.example.school.exception.ResourceNotFoundException;
import com.example.school.model.Class;
import com.example.school.service.ClassService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/classes")
public class ClassController {

    private ClassService classService;

    public ClassController(ClassService classService) {
        this.classService = classService;
    }

    @GetMapping("")
    public ResponseEntity<List<Class>> getClasss(){
        List<Class> classList = classService.findAll();
        return ResponseEntity.ok(classList);
    }

    @GetMapping("/{classId}")
    public ResponseEntity<Class> getClass(@PathVariable String classId){
        try{
            Class classs = classService.findById(classId);
            return ResponseEntity.ok(classs);}
        catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception f){
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("")
    public  ResponseEntity<Class> insertClass(@RequestBody Class classs){
        try{
            Class insertedClass = classService.insertClass(classs);
            return ResponseEntity.status(HttpStatus.CREATED).body(classs);}
        catch (ResourceAlreadyExistsException e) {return ResponseEntity.badRequest().body(null);}

    }

    @PutMapping("/{classId}")
    public  ResponseEntity<Class> updateClass(@PathVariable String classId, @RequestBody Class classs){
        try{
            Class updateClass = classService.updateClass(classId, classs);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(classs);}
        catch (ResourceNotFoundException e) {return ResponseEntity.badRequest().body(null);}

    }

    @DeleteMapping("/{classId}")
    public ResponseEntity<String> deleteClass(@PathVariable String classId){
        try{
            boolean deletedClass = classService.deleteById(classId);
            return ResponseEntity.ok(classId);}
        catch (ResourceNotFoundException e){return ResponseEntity.notFound().build();}
    }

}
