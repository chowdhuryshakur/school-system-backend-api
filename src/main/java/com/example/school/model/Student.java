package com.example.school.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"studentId", "fName"})
@Entity
public class Student {
    @Id
    private String studentId;
    private String fName;
    private String lName;
    private int rollNo;
    private Class cl;

}